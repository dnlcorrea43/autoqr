#from __future__ import print_function

from tkinter import *
from tkinter.font import Font
from datetime import datetime

import cv2, time, json
import pyzbar.pyzbar as pyzbar
import numpy as np
import PIL.Image, PIL.ImageTk
import pdb
from banco import Banco

# Tamanhos da Cam apropriados
#160.0 x 120.0
#176.0 x 144.0
#320.0 x 240.0
#352.0 x 288.0
#640.0 x 480.0
#1024.0 x 768.0
#1280.0 x 1024.0

# "Consts"
Instituicao = "UniAmérica"
FRAME_RATE = 25
BACKGROUND_COLOR="#0B396D"

class Marquee:
    def __init__(self, window):
        myFont = Font(family="Roboto", size=18)
        self.t = Text(window.left_pane, width=50, height=7,highlightthickness=0,borderwidth=0,background=BACKGROUND_COLOR)
        self.t.configure(font=myFont)

    def write(self, text):
        self.t.insert(INSERT, text);
        self.t.pack()

    def clear(self):
        self.t.delete('0.1', END)

    def blink(self, qrcode):
        self.clear()
        self.write("Seja bem-vindo à %s\nRA: %s\nData: %s\nPalestra_ID: %s"
                   % ( Instituicao, qrcode["ra"], datetime.now(), str(qrcode["palestra_id"]) ))
        self.t.pack()

class App:

    def ad(self):
        self.img = PIL.ImageTk.PhotoImage(PIL.Image.open("img/teste.png"))
        panel = Label(self.bottom_frame, image = self.img).pack(side='left')
    
    def __init__(self, window, window_title, video_source=0):
        #rgb(11, 57, 109)
        window.configure(background=BACKGROUND_COLOR)


        self.window = window

        # Frames
        self.top_frame = Frame(window)
        self.bottom_frame = Frame(window)
        self.top_frame.pack(side="top", fill="x", expand=True)
        self.bottom_frame.pack(side="bottom", fill="x", expand=True)

        # 
        self.right_pane = Frame(self.top_frame)
        self.left_pane = Frame(self.top_frame)

        self.right_pane.pack(side="right", fill="both", expand=True)
        self.left_pane.pack(side="left", fill="y", expand=True)

        
        self.window.title(window_title)
        self.video_source = video_source
        self.m = Marquee(self)

        # QR Code Field
        self.m.write( "Olá! Passe seu QRCode no leitor")

        self.leitura = ""
        self.counter = 0

        # open video source (by default this will try to open the computer webcam)
        self.vid = MyVideoCapture(self, self.video_source)

        # Create a canvas that can fit the above video source size
        self.canvas = Canvas(self.right_pane, width = self.vid.width, height = self.vid.height,
                             highlightthickness=0, borderwidth=0)
        self.canvas.pack(side='right')

        # Update the page
        self.update()

        ## Ads
        self.ad()

        self.window.mainloop()

    def reset(self):
        self.leitura = ''
        self.counter = 0
        self.m.clear()
        self.m.write("Olá! Passe seu QRCode no leitor")

    def update(self):
        # Get a frame from the video source
        ret, frame = self.vid.get_frame()

        if ret:
            self.photo = PIL.ImageTk.PhotoImage(image = PIL.Image.fromarray(frame))
            self.canvas.create_image(0, 0, image = self.photo, anchor = NW)

        self.window.after(FRAME_RATE, self.update)


class MyVideoCapture:
    def __init__(self, app, video_source=0):
        # Open the video source
        self.vid = cv2.VideoCapture(video_source)
        
        # Tamanhos da Cam apropriados
        #160.0 x 120.0
        #176.0 x 144.0
        #320.0 x 240.0
        #352.0 x 288.0
        #640.0 x 480.0
        #1024.0 x 768.0
        #1280.0 x 1024.0
        self.vid.set(3,176)
        self.vid.set(4,144)
        self.app = app;

        if not self.vid.isOpened():
            raise ValueError("Unable to open video source", video_source)

        # Get video source width and height
        self.width = self.vid.get(cv2.CAP_PROP_FRAME_WIDTH)
        self.height = self.vid.get(cv2.CAP_PROP_FRAME_HEIGHT)

    def decode(self, im): 
        decodedObjects = pyzbar.decode(im)
        return decodedObjects

    def get_frame(self):
        
        if self.vid.isOpened():
            ret, frame = self.vid.read()
            im = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

            if self.app.leitura: 
                self.app.counter += 25

                if (self.app.counter > 6000):
                    self.app.reset()

                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))

            decodedObjects = self.decode(im)

            # Nao roda o resto, decodedObjects
            # se retorna array vazio, ou seja, NO QR Code

            for decodedObject in decodedObjects: 
                points = decodedObject.polygon

                # If the points do not form a quad, find convex hull
                if len(points) > 4 : 
                    hull = cv2.convexHull(np.array([point for point in points], dtype=np.float32))
                    hull = list(map(tuple, np.squeeze(hull)))
                else : 
                    hull = points;

                # Number of points in the convex hull
                n = len(hull)     
                # Draw the convext hull
                for j in range(0,n):
                    cv2.line(frame, hull[j], hull[ (j+1) % n], (255,0,0), 3)

                x = decodedObject.rect.left
                y = decodedObject.rect.top

                j = json.loads( decodedObject.data.decode("utf-8") )

                Banco(j)

                self.app.leitura = str(j);

                self.app.m.blink(j)

            if ret:
                return (ret, cv2.cvtColor(frame, cv2.COLOR_BGR2RGB))
            else:
                return (ret, None)
        else:
            return (ret, None)

    # Release the video source when the object is destroyed
    def __del__(self):
        if self.vid.isOpened():
            self.vid.release()

# START
App(Tk(), "%s - Presença" % Instituicao)
