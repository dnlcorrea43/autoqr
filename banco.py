import sqlite3, MySQLdb

class Banco:
    def __init__(self, qrcode):
        self.qrcode = qrcode

        self.sqlite()
        self.mysql()


    def sqlite(self):
        query = "INSERT INTO leituras (ra, palestra_id, data) VALUES (?, ?, ?)"
        con = sqlite3.connect('autoqr.sqlite')
        cur = con.cursor()
        #cur.execute("""CREATE TABLE IF NOT EXISTS alunos (
        #            ra VARCHAR,
        #            palestra_id INTEGER,
        #            data DATETIME)""")
        try:
            cur.execute(query, (self.qrcode["ra"], self.qrcode["palestra_id"], 'NOW()'))
        except sqlite3.Error as e:
            print ("An error occurred:", e.args[0])

        con.commit()

    def mysql(self):
        db = MySQLdb.connect(host="localhost", user="autoqr", passwd="autoqr", db="autoqr")

        query = "INSERT INTO leituras (ra, palestra_id) VALUES (%s, %s)"

        insert = db.cursor()

        insert.execute(query, (self.qrcode["ra"], self.qrcode["palestra_id"]))

        db.commit()
